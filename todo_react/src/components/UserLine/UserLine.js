import React from "react";
import styles from "./userline.module.css";

import profilePicture from "../../img/perfect-location-for-a-profile-photo-2.jpeg";

export default function UserLine() {
  return (
    <a href="#0" className={styles["user-line"]}>
      <div className={styles["navigation-icon"]}>
        <img
          src={profilePicture}
          className={styles["profile-picture"]}
          alt="PP"
        />
      </div>
      <div className={styles["navigation-text"]}>Anton Borisov</div>
      <span
        className={styles["navigation-misc"]}
        role="img"
        aria-label="calendar"
      >
        🔍
      </span>
    </a>
  );
}
