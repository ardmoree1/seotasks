import React, { useContext } from "react";
import { Context } from "Contexts/Context";
import styles from "./NavigationList.module.css";

export default function NavigationList() {
  const { lists, setActiveList, setActiveTasks, onDeleteList } = useContext(
    Context
  );
  const handleActiveList = (item) => {
    setActiveList(item);
    setActiveTasks(item.tasks);
  };
  return (
    <React.Fragment>
      {lists.map((item) => (
        <a
          href="#0"
          key={item.id}
          className={styles["nav-link"]}
          onClick={() => handleActiveList(item)}
        >
          <span
            className={styles["navigation-icon"]}
            role="img"
            aria-label="calendar"
          >
            {item.icon}
          </span>
          <div className={styles["navigation-text"]}>{item.text}</div>
          <div className={styles["navigation-misc"]}>
            {item.tasks.length > 0 ? item.tasks.length : null}
          </div>
          <div
            className={styles["navigation-del"]}
            onClick={() => onDeleteList(item.id)}
          >
            <span>✖</span>
          </div>
        </a>
      ))}
    </React.Fragment>
  );
}
