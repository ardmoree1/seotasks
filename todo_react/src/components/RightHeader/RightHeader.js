import React, { useContext } from "react";
import styles from "./RightHeader.module.css";
import UserLine from "components/UserLine/UserLine";
import NewList from "components/NewList/NewList";
import NavigationList from "components/NavigationList/NavigationList";
import { Context } from "Contexts/Context";

export default function RightHeader() {
  const { activeList, lists } = useContext(Context);
  return (
    <header className={styles["right-header"]}>
      <label htmlFor={styles["toggle"]} className={styles["hamburger"]}>
        &#9776;
      </label>
      <input type="checkbox" id={styles["toggle"]} />
      <div className={styles["menu-mobile"]}>
        <UserLine />
        {lists && <NavigationList />}
        <NewList />
      </div>
      <div className={styles["empty"]}></div>
      {activeList && (
        <div className={styles["my-day-header"]}>{activeList.text}</div>
      )}
      {!activeList && (
        <div className={styles["my-day-header"]}>Welcome to your Todo app</div>
      )}
      <div className={styles["date-icon"]}>
        <time dateTime={Date()} className={styles["date"]}>
          {Date()}
        </time>
        <span className={styles["d-icon"]} role="img" aria-label="calendar">
          💡
        </span>
      </div>
    </header>
  );
}
