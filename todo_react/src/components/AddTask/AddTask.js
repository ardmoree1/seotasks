import React, { useState, useContext } from "react";
import { Context } from "Contexts/Context";
import styles from "./AddTask.module.css";

export default function AddTask() {
  const { onAddTask } = useContext(Context);
  const [inputValue, setInputValue] = useState("");

  const addTask = () => {
    if (inputValue) {
      onAddTask(inputValue);
      setInputValue("");
    }
  };
  const handleKeyDown = (e) => {
    if (e.key === "Enter" && inputValue) {
      addTask(inputValue);
    }
  };
  return (
    <div className={styles["add-task"]}>
      <input type="checkbox" className={styles["checkbox"]} />
      <input
        value={inputValue}
        onKeyDown={handleKeyDown}
        onChange={(e) => setInputValue(e.target.value)}
        type="text"
        placeholder="Add a to-do"
        className={styles["placeholder"]}
      />
      <button onClick={addTask} className={styles["add-task-btn"]}>
        Add
      </button>
    </div>
  );
}
