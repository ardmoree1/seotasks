import React, { useContext } from "react";
import RightHeader from "components/RightHeader/RightHeader";
import TaskList from "components/TaskList/TaskList";
import AddTask from "components/AddTask/AddTask";
import styles from "./RightPanel.module.css";
import { Context } from "Contexts/Context";

export default function RightPanel() {
  const { activeList } = useContext(Context);
  return (
    <div className={styles["right-panel"]}>
      <RightHeader />
      {activeList && <TaskList />}
      {activeList && <AddTask />}
      {!activeList && <h1 className={styles["start"]}>Select a list</h1>}
    </div>
  );
}
