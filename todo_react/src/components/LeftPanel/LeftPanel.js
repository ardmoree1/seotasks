import React from "react";
import Navigation from "components/Navigation/Navigation";
import NewList from "components/NewList/NewList";
import styles from "./LeftPanel.module.css";

export default function LeftPanel() {
  return (
    <div className={styles["left-panel"]}>
      <Navigation />
      <NewList />
    </div>
  );
}
