import React, { useContext } from "react";
import UserLine from "components/UserLine/UserLine";
import NavigationList from "components/NavigationList/NavigationList";
import { Context } from "Contexts/Context";

import styles from "./Navigation.module.css";

export default function Navigation() {
  const { lists } = useContext(Context);
  return (
    <nav className={styles["navigation"]}>
      <UserLine />
      {lists && <NavigationList />}
    </nav>
  );
}
