import React, { useState, Fragment, useContext } from "react";
import { Context } from "Contexts/Context";
import styles from "./NewList.module.css";

export default function NewList() {
  const { onAddList } = useContext(Context);
  const [visiblePopup, setVisiblePopup] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const addList = () => {
    if (inputValue) {
      onAddList(inputValue);
      setInputValue("");
      setVisiblePopup(!visiblePopup);
    } else {
      setVisiblePopup(false);
    }
  };
  const handleKeyDown = (e) => {
    if (e.key === "Escape") {
      setVisiblePopup(false);
    }
  };

  return (
    <Fragment>
      <div className={styles["new-list"]}>
        <span
          className={styles["navigation-icon"]}
          role="img"
          aria-label="calendar"
        >
          ➕
        </span>
        <div className={styles["navigation-text"]}>
          <button
            onClick={() => setVisiblePopup(!visiblePopup)}
            className={styles["add-btn"]}
          >
            New List
          </button>
        </div>
      </div>
      {visiblePopup && (
        <dialog open onKeyDown={handleKeyDown}>
          <form onSubmit={addList}>
            <input
              type="text"
              name="listName"
              value={inputValue}
              onChange={(e) => setInputValue(e.target.value)}
            ></input>
            <input type="button" value="Add" onClick={addList}></input>
          </form>
        </dialog>
      )}
    </Fragment>
  );
}
