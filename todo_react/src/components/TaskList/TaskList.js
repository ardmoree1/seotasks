import React, { Fragment, useContext } from "react";
import styles from "./TaskList.module.css";
import { Context } from "Contexts/Context";

export default function TaskList() {
  const { activeTasks, onDeleteTask } = useContext(Context);
  return (
    <ul className={styles["task-list"]}>
      {activeTasks.map((tasks) => (
        <li className={styles["task-entry"]} key={tasks.id}>
          <input
            type="checkbox"
            className={styles["checkbox"]}
            id={`checkbox${tasks.id}`}
          />
          <div className={styles["task-content"]}>
            <label htmlFor={`checkbox${tasks.id}`}>{tasks.label}</label>
            <div className={styles["notes"]}>
              <div className={styles["todo-task"]}>{tasks.listTitle}</div>
              {tasks.day ? (
                <Fragment>
                  {" "}
                  <div className={styles["todo-dot"]}>&#9679;</div>
                  <span
                    className={styles["day-icon"]}
                    role="img"
                    aria-label="calendar"
                  >
                    📅
                  </span>
                  <span className={styles["label-tomorrow"]}>{tasks.day}</span>
                </Fragment>
              ) : null}
            </div>
          </div>
          <span
            className={styles["delete-task"]}
            onClick={() => onDeleteTask(tasks.id)}
          >
            ✖
          </span>
        </li>
      ))}
    </ul>
  );
}
