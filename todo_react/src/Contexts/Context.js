import React, { createContext, useState, useEffect } from "react";
import axios from "axios";

export const Context = createContext();

const ContextProvider = (props) => {
  const [lists, setLists] = useState(null);
  const [activeList, setActiveList] = useState(null);
  const [activeTasks, setActiveTasks] = useState(null);

  useEffect(() => {
    axios.get("http://localhost:3001/lists?_embed=tasks").then(({ data }) => {
      setLists(data);
    });
  }, []);
  console.log(lists);
  const onAddList = (txt) => {
    const newList = [
      ...lists,
      {
        icon: "↔",
        text: txt,
        counter: null,
        tasks: [],
        size: lists.tasks.length(),
      },
    ];
    axios.post("http://localhost:3001/lists?_embed=tasks", {
      icon: "↔",
      text: txt,
      counter: null,
      tasks: [],
      size: lists.tasks.length(),
    });
    setLists(newList);
  };
  const onDeleteList = (id) => {
    const newList = lists.filter((list) => list.id !== id);
    axios.delete("http://localhost:3001/lists/" + id).catch(() => {
      alert("Failed to delete a list");
    });
    setLists(newList);
  };
  const onAddTask = (task) => {
    const newTasks = [
      ...activeTasks,
      {
        label: task,
        listTitle: activeList.text,
        day: Date(),
        listId: activeList.id,
      },
    ];
    axios.post("http://localhost:3001/tasks", {
      label: task,
      listTitle: activeList.text,
      day: Date(),
      listId: activeList.id,
    });
    setActiveTasks(newTasks);
  };
  const onDeleteTask = (id) => {
    const newTasks = activeTasks.filter((task) => task.id !== id);
    axios.delete("http://localhost:3001/tasks/" + id).catch(() => {
      alert("Failed to delete a list");
    });
    setActiveTasks(newTasks);
  };

  return (
    <Context.Provider
      value={{
        lists,
        setLists,
        activeList,
        setActiveList,
        activeTasks,
        setActiveTasks,
        onAddList,
        onAddTask,
        onDeleteList,
        onDeleteTask,
      }}
    >
      {props.children}
    </Context.Provider>
  );
};

export default ContextProvider;
