import RightPanel from "components/RightPanel/RightPanel";
import LeftPanel from "components/LeftPanel/LeftPanel";
import React from "react";
import "./index.css";
import ContextProvider from "Contexts/Context";

function App() {
  return (
    <div className="main">
      <ContextProvider>
        <LeftPanel />
        <RightPanel />
      </ContextProvider>
    </div>
  );
}

export default App;
